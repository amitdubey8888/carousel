var express = require('express')
   , router = express.Router()
   , MongoClient = require('mongodb').MongoClient
   , ObjectId = require('mongodb').ObjectId
   , fs = require('fs-extra')
   , url = 'mongodb://localhost:27017/carousel'
   , multer = require('multer')
   , util = require('util')
   , upload = multer({limits: {fileSize: 2000000 },dest:'/uploads/'})
 
router.get('/', function(req, res){ res.render('index'); });
 
router.post('/', upload.single('picture'), function (req, res){
 
	if(req.file == null)
	{
	  res.render('index', { title:'Please select a picture file to submit!'});
	} 
	else 
	{
		MongoClient.connect(url, function(err, db){
		   var newImg = fs.readFileSync(req.file.path);
		   var encImg = newImg.toString('base64');
		   var newItem = {
		      ModelName: req.body.modal,
		      color: 'green',
		      Brand: req.body.brand,
		      url: Buffer(encImg, 'base64'),
		      Created_date:new Date(2015,1,3,15,30).toLocaleString()
		   };
		 
			db.collection('formdata').insert(newItem, function(err, result){
				if (err) { console.log(err); };
				var newoid = new ObjectId(result.ops[0]._id);
				fs.remove(req.file.path, function(err) {
					if (err) { console.log(err) };
					res.render('index', {title:'Thanks for the Picture!'});
				});
			});
	   });
	};
});

router.get('/allpicture', function(req, res){
   MongoClient.connect(url, function(err, db){
	db.collection('formdata').
	find({}).toArray(function (err,result) {
        if(err){
            return res.json(err);
        }
        else{
           return res.json(result);
        }
    })
   });
});

router.get('/picture', function(req, res){
   MongoClient.connect(url, function(err, db){
		var mysort = { Created_date: -1 };
		db.collection("formdata").find().sort(mysort).limit(1).toArray(function(err, result) {
			if(err){
	            return res.json(err);
	        }
	        else{
	           return res.json(result);
	        }
		});
     });
});

module.exports = router;